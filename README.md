# Laravel 勉強会

## PHP Framework Evolution

  ![PHP Framework Evolution](https://image.slidesharecdn.com/phpstudy91-150622151040-lva1-app6892/95/91-php-phpstudy-13-638.jpg?cb=1435070589)

## サービスコンテナ

以下の２つの機能がある

  - インスタンスを返してくれる機能
      - サービスコンテナへインスタンスの生成方法を登録すること
          - binding/バインドする
      - サービスコンテナが指定されたインスタンスを生成して返すこと
          - resolving/解決する

  - DI (Dependency Injection/依存性の注入) コンテナ機能
      - 「依存性の注入」は訳が良くない。「依存オブジェクト注入」が適切。
      - あるクラスが必要としている（依存している）別のクラスのインスタンスを、  
        そのクラスの外部からコンストラクタやメソッドの引数などの形で渡す（注入する）という概念の実装

## IoC (Inversion of Control/制御の反転)
  - 包括的な概念
  - 「設定を利用から分離する」原則
  - 具体的な実装が「DIコンテナ」

## サービスコンテナの使い方
  - サービスコンテナにインスタンスの生成方法をバインドする際のキーとして文字列を指定する
      - どんな文字列でも良いが、通常はクラスやインターフェースの完全修飾名を指定する
      - DIコンテナ機能を使用する場合はタイプヒンティングのために完全修飾名でバインドする必要がある
      - `Hoge::class` で完全修飾名を取得できる
  - キーを指定することで、あらかじめバインドしておいた生成方法に従って生成されたインスタンスをサービスコンテナから取得できる

## 何故 DI コンテナ機能を使うのか
  - クラス間が疎結合になり、再利用性が高まる
  - 依存クラスをモックに差し替えられるので、ユニットテストがしやすくなる

## DIパターン/DIコンテナ/サービスロケータパターンの違い

  [http://blog.a-way-out.net/blog/2015/08/31/your-dependency-injection-is-wrong-as-I-expected/](http://blog.a-way-out.net/blog/2015/08/31/your-dependency-injection-is-wrong-as-I-expected/)

## サービスプロバイダ

  - フレームワークやアプリケーションに含まれる各コンポーネントの起動処理を行うためのクラス
      - サービスコンテナへのバインド
      - イベントリスナーの登録
      - ミドルウェアの登録
      - ルーティングの登録

## Laravel 5 の処理の流れ

  [https://www.pupha.net/archives/3008/](https://www.pupha.net/archives/3008/)

## Laravel の書籍 まとめ

  [http://qiita.com/mukaken/items/2058a193ee0ce6fbfd30](http://qiita.com/mukaken/items/2058a193ee0ce6fbfd30)